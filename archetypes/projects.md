---
title: "{{ replace .Name "-" " " | title }}"
dates: Start to end
client: Faculty, department, project name
draft: true
authors:
  - Author One
  - Author Two
summary: Short description of the project (optional)
---

This is an introductory paragraph that briefly describes the project.  If the
summary isn't defined in the front matter, either the first 70-80 words will
be used or the text up to the marker below.

<!--more-->

## How we became involved

Sections should start at heading level 2.  These section headers aren't
necessary but might be a good start.

## Our contributions

Here we describe how we're helping the researchers achieve their goals.
