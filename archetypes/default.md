---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
---

This is the text that appears before the summary.  Note the title is not
included--that should be in the layout, either for the theme or the individual
site.

<!--more-->

Anything appearing after that splitter will not appear in summaries but will
of course appear in the entire article.

If you want to have a summary that is NOT part of the article, remove the
summary marker and add a summary field to the front matter, above.
