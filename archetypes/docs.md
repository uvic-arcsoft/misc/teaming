---
title: "{{ replace .Name "-" " " | title }}"
draft: true
authors:
  - Author One
  - Author Two
summary: Short description of the document (optional)
---

This is an introductory paragraph that briefly describes the document.  If the
summary isn't defined in the front matter, either the first 70-80 words will
be used or the text up to the marker below.

<!--more-->

## First section

Sections should start at heading level 2.  These section headers aren't
necessary but might be a good start.

## Another section

This could be anything.
