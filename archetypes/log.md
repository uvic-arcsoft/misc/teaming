---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
authors: []
summary: (optional) This summary will override anything appearing in the summary section
---

This is an introductory paragraph which, if there is no `summary` field
defined in the front matter, will be used up to the configured limit of words
or the marker which appears below.

<!--more-->

Everything after it will not be part of the summary.

## Guidelines for new blog posts

Write content using Markdown and observe the following guidelines:

- Do not include the title in the content of your post.  The layout will take
  care of that.
- Use heading of level 2 (`##`, `###`, etc.) and higher.  Level 1 should be
  reserved for the layout to use for the title.
- For code snippets, if possible keep them at a reasonable width so that side
  scrolling may be avoided.  Long commands can be split with `\` (so long as
  it is still valid syntax).  Long output can be expurgated of unnecessary
  content that doesn't provide useful context.

