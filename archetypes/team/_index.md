---
name: {{ replace .Name "-" " " | title }}
twitter:
mastodon:
linkedin:
photo: (image in author directory)
term: Start to End
summary:
---

Stuff about the author.

No need to include name in title heading, stuff about their articles and so on
as these are listed automatically.
