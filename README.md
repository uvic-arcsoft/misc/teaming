# Teaming theme for Hugo

Teaming aims to be a simple and clean Hugo theme for developing websites for
teams.  The original audience and use case is a software development team
that needs to describe its services, ongoing projects, and other common stuff,
and additionally featuring a blog to which all members are expected to
contribute.

## Status

- Not documented
- Not doing anything for inclusion with [Hugo
  themes](https://themes.gohugo.io)

## Building a site

### Banner image

To provide a banner image, create an appropriately sized graphic of an
appropriate format (PNG and SVG should both work, others probably) and place
it in `assets/images/banner.<ext>` where `<ext>` is `.png`, `.svg`, or
whatever extension is appropriate.
