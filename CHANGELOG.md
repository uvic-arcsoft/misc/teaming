# Changelog

## v0.3 (20240830) Added Mermaid

Added Mermaid for diagrams

## v0.2 (20230914) Updates

Presentation updates

## v0.1 (20230211) Essential functionality

Basically working for both desktop and mobile
